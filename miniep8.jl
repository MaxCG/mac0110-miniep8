function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

function compareNum(a, b)
	valor = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
	x = a[1:(end-1)]
	y = b[1:(end-1)]
	for el in valor
		if el == x
			if el == y
				return false
			end
			return true
		end
		if el == y
			return false
		end
	end
	error("Caracter invalido")
end

function compareSuit(a, b)
	valor = ['♦', '♠', '♥', '♣']
	x = a[end]
	y = b[end]

	for el in valor
		if el == x
			if el == y
				return 0
			end
			return 1
		end
		if el == y
			return -1
		end
	end
	error("Caracter invalido")

end

function compareByValue(x, y)
	# true = x < y
	temp = compareSuit(x, y)

	if temp == 0
		return compareNum(x, y)
	elseif temp == 1
		return true
	else
		return false
	end
end

function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValue(v[j], v[j - 1])
				troca(v, j, j-1)
			else
				break
			end
			j -= 1
		end
	end
	return v
end
