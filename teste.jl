include("./miniep8.jl")

using Test

function testNum()
	println("[Num] Testing")
	@test compareNum("2N", "AN")
	@test compareNum("7N", "KN")
	@test !compareNum("8N", "2N")
	@test !compareNum("3N", "3N")
	@test compareNum("10N", "JN")
	println("[Num] OK")
end

function testSuit()
	#  ♦<♠<♥<♣
	println("[Suit] Testing")
	@test compareSuit("♦", "♠") == 1
	@test compareSuit("♥", "♣") == 1
	@test compareSuit("♥", "♦") == -1
	@test compareSuit("♦", "♦") == 0
	println("[Suit] OK")
	
end

function testInsert()
	println("[Insert] Testing")
	@test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
	println("[Insert] OK")
end

function test()
	testNum()
	testSuit()
	testInsert()
end

test()
